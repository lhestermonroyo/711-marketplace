import React from 'react';
import { Container, Card, Row, Col, Image, Button } from 'react-bootstrap';
import { items } from '../../mocks';
import StarRating from '../../components/StarRating';

const Store = (props) => {
  const { history } = props;
  
  return (
    <Container style={{ marginTop: '7rem' }}>
      <div className="float-right">
        <Button className="mb-3" variant="outline-primary"><i className="fa fa-filter fa-lg fa-fw"/></Button>
        <Button className="mb-3 ml-2" variant="outline-primary"><i className="fa fa-grip-horizontal fa-lg fa-fw"/></Button>
      </div>
      <div className="clearfix"/>
      <Row>
      {items.map((item, i) => {
        return (
          <Col lg={4} className="mb-2">
            <Card bg="light" key={i}>
              <Image style={{cursor: 'pointer'}} onClick={() => history.push(`/item-details/${item.id}`)} src={item.item_photos[0]} fluid />
              <Card.Body style={{cursor: 'pointer'}} onClick={() => history.push(`/item-details/${item.id}`)}>
                <p className="mb-1"><StarRating rating={item.item_rating}/></p>
                <h5>{item.item_name}</h5>
                <hr/>
                <h5 className="text-secondary">{`P${item.item_price}`}</h5>
              </Card.Body>
              <Button className="btn-block" onClick={() => history.push(`/cart/${item.id}`)}>Add to Cart <i className="fa fa-shopping-cart fa-fw"/></Button>
            </Card>
          </Col>
        )
      })
      }
      </Row>
    </Container>
  )
}

export default Store;