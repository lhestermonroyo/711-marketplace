export const items = [
  {
    id: 1,
    sellerId: 1,
    categoryId: 1,
    item_name: "iPhone X",
    item_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    item_price: "300",
    item_photos: [
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
    ],
    item_rating: 4,
    timestamp: "2019-03-17 21:40:03",
  },
  {
    id: 2,
    sellerId: 5,
    categoryId: 2,
    item_name: "Mitsubishi Montero Sports",
    item_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    item_price: "500",
    item_photos: [
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
    ],
    item_rating: 5,
    timestamp: "2019-03-17 21:40:03",
  },
  {
    id: 3,
    sellerId: 7,
    categoryId: 1,
    item_name: "Macbook Pro 2019",
    item_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    item_price: "275",
    item_photos: [
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
    ],
    item_rating: 4.7,
    timestamp: "2019-03-17 21:40:03",
  },
  {
    id: 3,
    sellerId: 10,
    categoryId: 3,
    item_name: "Samsung Smart TV 2019",
    item_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    item_price: "700",
    item_photos: [
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
    ],
    item_rating: 4.3,
    timestamp: "2019-03-17 21:40:03",
  },
  {
    id: 4,
    sellerId: 7,
    categoryId: 5,
    item_name: "Buffalo Wings",
    item_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    item_price: "1000",
    item_photos: [
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
    ],
    item_rating: 3,
    timestamp: "2019-03-17 21:40:03",
  },
  {
    id: 5,
    sellerId: 8,
    categoryId: 2,
    item_name: "Nissan Almera 2019",
    item_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    item_price: "500",
    item_photos: [
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
    ],
    item_rating: 4.5,
    timestamp: "2019-03-17 21:40:03",
  },
  {
    id: 6,
    sellerId: 6,
    categoryId: 5,
    item_name: "Vanilla Cakes",
    item_description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
    item_price: "5000",
    item_photos: [
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
      "https://dummyimage.com/600x400/aba4ab/fff",
    ],
    item_rating: 4,
    timestamp: "2019-03-17 21:40:03",
  }
];


export const categories = [
  {
    id: 1,
    name: "Gadgets and Technology",
  },
  {
    id: 2,
    name: "Automobile",
  },
  {
    id: 3,
    name: "Appliances",
  },
  {
    id: 4,
    name: "Clothing and Fashion",
  },
  {
    id: 5,
    name: "Foods and Spices",
  },
  {
    id: 6,
    name: "Real Estate"
  },
  {
    id: 7,
    name: "Furnitures",
  },
];

export const sellers =[
  {
    "id": 1,
    "fullname": "Luther Spight",
    "username": "lspight0",
    "email": "lspight0@youku.com",
    "identity_status": true,
    "avatar": "https://robohash.org/velitsedperferendis.png?size=50x50&set=set1",
    "rating": 2
  }, {
    "id": 2,
    "fullname": "Gleda Amer",
    "username": "gamer1",
    "email": "gamer1@nih.gov",
    "identity_status": true,
    "avatar": "https://robohash.org/experspiciatisodit.png?size=50x50&set=set1",
    "rating": 3
  }, {
    "id": 3,
    "fullname": "Kaitlynn Gerhardt",
    "username": "kgerhardt2",
    "email": "kgerhardt2@hc360.com",
    "identity_status": true,
    "avatar": "https://robohash.org/quasdoloreest.png?size=50x50&set=set1",
    "rating": 1
  }, {
    "id": 4,
    "fullname": "Belita Jeffree",
    "username": "bjeffree3",
    "email": "bjeffree3@diigo.com",
    "identity_status": true,
    "avatar": "https://robohash.org/veldoloresmaiores.png?size=50x50&set=set1",
    "rating": 1
  }, {
    "id": 5,
    "fullname": "Gilberta Drinkel",
    "username": "gdrinkel4",
    "email": "gdrinkel4@cdbaby.com",
    "identity_status": false,
    "avatar": "https://robohash.org/etsedmagni.png?size=50x50&set=set1",
    "rating": 5
  }, {
    "id": 6,
    "fullname": "Amelina Roughsedge",
    "username": "aroughsedge5",
    "email": "aroughsedge5@chronoengine.com",
    "identity_status": true,
    "avatar": "https://robohash.org/optioetsed.png?size=50x50&set=set1",
    "rating": 4
  }, {
    "id": 7,
    "fullname": "Malena Hovington",
    "username": "mhovington6",
    "email": "mhovington6@furl.net",
    "identity_status": true,
    "avatar": "https://robohash.org/remquamea.png?size=50x50&set=set1",
    "rating": 2
  }, {
    "id": 8,
    "fullname": "Emelen Paulet",
    "username": "epaulet7",
    "email": "epaulet7@washington.edu",
    "identity_status": false,
    "avatar": "https://robohash.org/delenitianimivoluptatem.png?size=50x50&set=set1",
    "rating": 2
  }, {
    "id": 9,
    "fullname": "Susannah Aumerle",
    "username": "saumerle8",
    "email": "saumerle8@taobao.com",
    "identity_status": true,
    "avatar": "https://robohash.org/possimussedexercitationem.png?size=50x50&set=set1",
    "rating": 5
  }, {
    "id": 10,
    "fullname": "Pepita Wyeld",
    "username": "pwyeld9",
    "email": "pwyeld9@mail.ru",
    "identity_status": true,
    "avatar": "https://robohash.org/estetrerum.png?size=50x50&set=set1",
    "rating": 4
  },
];