import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Store from '../pages/Store';
import Cart from '../pages/Cart';
import Checkout from '../pages/Checkout';
import Wallet from '../pages/Wallet';

export const routes = (
  <Switch>
    <Route exact path="/" component={Store}/>
    <Route exact path="/cart/:id" component={Cart}/>
    <Route exact path="/checkout" component={Checkout}/>
    <Route exact path="/wallet" component={Wallet}/>
  </Switch>
);