import React from 'react';

const StarRating = (props) => {
  const { rating } = props;
  const color = { color: 'yellow' };
  return (
    <>
      {rating === 5 ? 
      <>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        {` (${rating})`}
      </>
      : rating < 5 || rating > 4.5 ? 
      <>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star-half fa-fw" style={color}/>
        {` (${rating})`}
      </>
      : rating === 4 ? 
      <>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        {` (${rating})`}
      </>
      : rating === 3 ? 
      <>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        <i className="fa fa-star fa-fw" style={color}/>
        {` (${rating})`}
      </>
      : null
      }
    </>
  )
}

export default StarRating;