import React, { useState, useEffect } from 'react';
import { useStoreState, useStoreActions } from 'easy-peasy';
import { Card, Form, Button, Row, Col, Image, Table } from 'react-bootstrap';
import { itemIdToItem, categoryIdToCategory } from '../../utils/IdToData';
import StarRating from '../../components/StarRating';

const CheckoutComponents = (props) => {
  const [ toggle, setToggle ] = useState(false);
  const { eventKey, setStep } = props;
  const { cart, balance, userData, cartAmountTotals } = useStoreState(state => state);
  const { fetchBalance, transferBalance } = useStoreActions(action => action);
  let total = cartAmountTotals.reduce((a, b) => a + b, 0);

  useEffect(() => {
    fetchBalance('a@a.com');
  }, []); //eslint-disable-line

  const handleSubmit = () => {
    const transferData = { senderEmail: userData.email, receiverEmail: 'b@b.com', amount: total };
    transferBalance(transferData);
    alert('Order has been placed and the seller receives the payment.');
  };

  return (
    <Card>
      <Card.Body>
      {eventKey === 1 ?
        <Form onSubmit={(e) => {
          e.preventDefault();
          setStep(2);
        }}>
          <h3>Costumer Information</h3>
          <p className="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          {userData ?
          <Row>
            <Col lg={6}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Firstname</Form.Label>
                <Form.Control type="text" value={userData.firstName}/>
              </Form.Group>
            </Col>
            <Col lg={6}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Lastname</Form.Label>
                <Form.Control type="text" value={userData.lastName} />
              </Form.Group>
            </Col>
            <Col lg={6}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>E-mail</Form.Label>
                <Form.Control type="email" value={userData.email} />
              </Form.Group>
            </Col>
            <Col lg={6}>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Phone Number </Form.Label>
                <Form.Control type="text" value="0924234233" />
              </Form.Group>
            </Col>
            <Col lg={6}></Col>
          </Row>
          : null
          }
          
          <Button variant="primary" type="submit">
            Submit and Continue <i className="fa fa-chevron-right fa-fw"/>
          </Button>
        </Form>
      : eventKey === 2 ? 
        <Form onSubmit={(e) => {
          e.preventDefault();
          setStep(3);
        }}>
          <h3>Delivery Details</h3> 
          <p className="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>         
          <Form.Group controlId="formBasicEmail">
            <Form.Label>Delivery Address</Form.Label>
            <Form.Control as="textarea" value="Test Address" />
          </Form.Group>
          <Form.Group controlId="formBasicPassword">
            <Form.Label>Address Landmark (optional)</Form.Label>
            <Form.Control type="text" value="Test Landwork"/>
          </Form.Group>
          <Button variant="primary" type="submit">
            Submit and Continue <i className="fa fa-chevron-right fa-fw"/>
          </Button>
        </Form>
      : eventKey === 3 ? 
        <Form onSubmit={(e) => {
          e.preventDefault();
          setStep(4);
        }}>
          <h3>Choose Payment Option</h3>          
          <p className="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          <Row className="mb-3">
            <Col lg={4}>
              <Card onClick={() => setToggle(!toggle)}>
                <Card.Body>
                  <p className="text-center text-primary mt-2">
                    <i className="fa fa-dollar-sign fa-4x fa-fw"/>
                  </p>
                  <p className="text-center">Stablecoin</p>
                  <p className="text-center mt-1"><Form.Check type="radio" onClick={() => setToggle(!toggle)} name="paymentOption" aria-label="radio 1" /></p>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={4}>
              <Card style={{cursor: 'pointer'}}>
                <Card.Body>
                  <p className="text-center text-primary mt-2">
                    <i className="fa fa-credit-card fa-4x fa-fw"/>
                  </p>
                  <p className="text-center">Credit/Debit Card</p>
                  <p className="text-center mt-1"><Form.Check type="radio" name="paymentOption" aria-label="radio 1" /></p>
                </Card.Body>
              </Card>
            </Col>
            <Col lg={4}>
            <Card style={{cursor: 'pointer'}}>
              <Card.Body>
                <p className="text-center text-primary mt-2">
                  <i className="fa fa-money-bill fa-4x fa-fw"/>
                </p>
                <p className="text-center">Cash on Delivery</p>
                <p className="text-center mt-1"><Form.Check type="radio" name="paymentOption" aria-label="radio 1" /></p>
              </Card.Body>
            </Card>
            </Col>
          </Row>
          <hr/>
          <div>
          {toggle ? 
            <>
            <p className="mb-1">Stablecoin Balance:</p>
            <h5 className="text-secondary mb-3">{balance.balance} SC</h5>
            <Button variant="primary" type="submit">
              Continue to Order Review <i className="fa fa-chevron-right fa-fw"/>
            </Button>
            </>
          : null
          }
          </div>            
        </Form>
      : eventKey === 4 ?
        <div>
        <h3>Order Review</h3>          
          <p className="mb-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
          {cart ? 
            cart.map((cartData, i) => {
              const itemData = itemIdToItem(cartData.itemId);
              const catData = categoryIdToCategory(itemData.categoryId);
              return (
                <Card key={i} className="mb-2">
                  <Card.Body>
                    <Row>
                      <Col lg={3}>
                        <Image src={itemData.item_photos[0]} fluid />
                      </Col>
                      <Col lg={6}>
                        <p className="mb-1"><StarRating rating={itemData.item_rating}/></p>
                        <h3>{itemData.item_name}</h3>
                        <h5 className="text-secondary">{`P${itemData.item_price}`} &bull; {catData.name}</h5>
                      </Col>
                      <Col lg={3}>
                        <p className="mb-1">Quantity</p>
                        <p className="text-secondary mb-1">{cartData.quantity}</p>
                        <p className="mb-1">Total Amount</p>
                        <p className="text-secondary mb-1">P{cartData.totalAmount}</p>
                      </Col>
                    </Row>
                  </Card.Body>
                </Card>
              )
            })
          : null
          } 
          {userData ?
          <>
            <h5 className="mt-3">Order Total</h5>
            <Table responsive>
              <tbody>
                <tr>
                  <td>Order total</td>
                  <td>{total}</td>
                </tr>
              </tbody>
            </Table>
            <h5 className="mt-3">Payment Method</h5>
            <Table responsive>
              <tbody>
                <tr>
                  <td>Payment Option</td>
                  <td>Stablecoin</td>
                </tr>
                <tr>
                  <td>Balance Remaining</td>
                  <td>{balance.balance} SC</td>
                </tr>
              </tbody>
            </Table>
            <h5 className="mt-3">Costumer Details</h5>
            <Table responsive>
              <tbody>
                <tr>
                  <td>Firstname</td>
                  <td>{userData.firstName}</td>
                </tr>
                <tr>
                  <td>Lastname</td>
                  <td>{userData.lastName}</td>
                </tr>
                <tr>
                  <td>E-mail</td>
                  <td>{userData.email}</td>
                </tr>
                <tr>
                  <td>Phone</td>
                  <td>0924234233</td>
                </tr>
              </tbody>
            </Table>
          </>
          : null
          }
          <h5 className="mt-3">Costumer Details</h5>
          <Table responsive>
            <tbody>
              <tr>
                <td>Delivery Address</td>
                <td>Test Address</td>
              </tr>
              <tr>
                <td>Delivery Landmark</td>
                <td>Test Landmark</td>
              </tr>
            </tbody>
          </Table>
          <Button onClick={() => handleSubmit()} className="mt-3">Place your Order <i className="fa fa-chevron-right fa-fw"/></Button>
        </div>
      : null
      }
      </Card.Body>
    </Card>
  )
}

export default CheckoutComponents;