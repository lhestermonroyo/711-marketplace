import React, { useEffect } from 'react';
import { useStoreActions, useStoreState } from 'easy-peasy';
import { Navbar, Nav, NavDropdown, Container, Image, Badge } from 'react-bootstrap';


const Header = () => {
  const { userData, cart } = useStoreState(state => state);
  const { fetchUser } = useStoreActions(action => action);
  const imgAddress = "https://store.playstation.com/store/api/chihiro/00_09_000/container/GB/en/999/EP4510-NPEB02098_00-AVCAPABILT000001/1550717678000/image?w=240&h=240&bg_color=000000&opacity=100&_version=00_09_000";
  const blankImgAddress = "https://images.homedepot-static.com/productImages/0b10f2de-892e-42b7-aed4-6fa738027a16/svn/storm-matte-formica-laminate-sheets-009121258512000-64_400_compressed.jpg";
  
  useEffect(() => {
    fetchUser();
  }, []); //eslint-disable-line

  console.log(userData);
  console.log(cart);
  return (
    <Navbar collapseOnSelect expand="lg" className="mb-5" bg="light" variant="light" fixed="top">
      <Container>
        <Navbar.Brand href="#/"><i className="fa fa-store"/> Aftermarket</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto"></Nav>
          <Nav>
            {userData ? 
              <NavDropdown 
                title={
                  <span>
                    {`${userData.email}`}
                    <Image 
                      className="float-right"
                      style={{height: 45, width: 45, marginTop: -15, marginLeft: '0.5rem'}}
                      src={imgAddress} 
                      roundedCircle 
                      thumbnail/>
                  </span>
                } 
                id="collasible-nav-dropdown">
                <NavDropdown.Item href="">{`${userData.firstName} ${userData.lastName}`}</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#/wallet">My Wallet</NavDropdown.Item>
                <NavDropdown.Item href="">Account Settings</NavDropdown.Item>
                <NavDropdown.Item href="#/login">Log Out</NavDropdown.Item>
              </NavDropdown>
            : <NavDropdown 
            title={
              <span>
                {'Please Wait...'}
                <Image 
                  className="float-right"
                  style={{height: 45, width: 45, marginTop: -15, marginLeft: '0.5rem'}}
                  src={blankImgAddress} 
                  roundedCircle 
                  thumbnail/>
              </span>
            } 
            id="collasible-nav-dropdown">
            <NavDropdown.Item href="">{`Please wait...`}</NavDropdown.Item>
            <NavDropdown.Divider />
            <NavDropdown.Item href="">Account Settings</NavDropdown.Item>
            <NavDropdown.Item href="">Log Out</NavDropdown.Item>
          </NavDropdown>
            }
          </Nav>
          <Nav.Link style={{color: '#101010'}} href="#/">
            <i className="fa fa-search fa-lg fa-fw"/>
          </Nav.Link>
          <Nav.Link style={{color: '#101010'}} href="#/">
            <i className="fa fa-shopping-bag fa-lg fa-fw"/>
            <Badge pill className="float-right" variant="danger">
              {cart.length !== 0 ? cart.length : null}
            </Badge>
          </Nav.Link>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  )
}

export default Header;
