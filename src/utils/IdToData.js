import { categories, items } from '../mocks';

export const categoryIdToCategory = (id) => {
  const category = categories.find(category => category.id === id);
  return category;
}

export const itemIdToItem = (id) => {
  const item = items.find(item => item.id === id);
  return item;
}